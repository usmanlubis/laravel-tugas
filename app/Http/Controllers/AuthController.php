<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request['nameFirst'];
        $namaBelakang = $request['nameLast'];
        return view('/welcome', compact('namaDepan', 'namaBelakang'));
    }
}
