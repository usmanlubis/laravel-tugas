@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstName">First Name: </label><br><br>
        <input type="text" name="nameFirst" id="firstName" required><br><br>
        <label for="lastName">Last Name :</label><br><br>
        <input type="text" name="nameLast" id="lastName" required><br><br>
        <label for="gender">Gender</label><br><br>
        <input type="radio" id="male" name="gender"><label for="male">Male</label><br>
        <input type="radio" id="female" name="gender"><label for="female">Female</label><br><br>
        <label for="state">Nationalitty</label><br><br>
        <select name="nationalitty" id="state" required>
            <option value="Indonesai">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="biodata" id="bio" cols="30" rows="10" required></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection